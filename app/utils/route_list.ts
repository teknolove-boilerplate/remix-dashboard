export const PageLimit = "?page=1&limit=10";
export const PageUnlimited = "?page=1&limit=1000";
export const PageHome = "/";
export const PageUsers = "/users";
export const PageForm ="/form"
