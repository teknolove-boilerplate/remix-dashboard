import { Dashboard } from "~/components/dashboard/dashboard";

export default function Index() {
  return (
    <>
      <Dashboard
        mainContent={
          <>
            Users
          </>
        }
      />
    </>
  );
}
