import { Dashboard } from "~/components/dashboard/dashboard";
import { DatePickerDemo } from "~/components/date_picker_demo/date_picker_demo";
import { Button } from "~/components/ui/button";
import { useToast } from "~/components/ui/use-toast";

export default function Index() {
  const { toast } = useToast();
  return (
    <>
      <Dashboard
        mainContent={
          <>
            <div>Form</div>
            <DatePickerDemo />
            <Button
              onClick={() => {
                toast({
                  className:
                    "top-0 right-0 flex fixed md:max-w-[420px] md:top-4 md:right-4",
                  title: "Aku adalah toast",
                  description: "deskripsi..",
                });
              }}
            >
              Click
            </Button>
          </>
        }
      />
    </>
  );
}
