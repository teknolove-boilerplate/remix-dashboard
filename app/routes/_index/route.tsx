import { LoaderFunction } from "@remix-run/node";
import { SAMPLE_API } from "~/api/SAMPLE_API";
import { Dashboard } from "~/components/dashboard/dashboard";
export const loader: LoaderFunction = async  () => {
  const res = await SAMPLE_API()
  console.log(res,"RES DATA"); 
  return null;
};
export default function Index() {
  return (
    <>
      <Dashboard
        mainContent={
          <>
            <section>
              Beranda
            </section>
          </>
        }
      />
    </>
  );
}
