import {
  HomeIcon,
  IdentificationIcon,
  UsersIcon,
} from "@heroicons/react/24/outline";
import { PageForm, PageHome, PageUsers } from "~/utils/route_list";

export const DashboardMenu = [
  { name: "Home", href: PageHome, icon: HomeIcon },
  { name: "Users", href: PageUsers, icon: UsersIcon },
  { name: "Forms", href: PageForm, icon: IdentificationIcon },
];

export const DashboardTeams = [
  { id: 1, name: "Team One", href: "/users/one", initial: "1", current: false },
  { id: 2, name: "Team Two", href: "/users/two", initial: "2", current: false },
  { id: 3, name: "Team Three", href: "/users/three", initial: "3", current: false },
];
